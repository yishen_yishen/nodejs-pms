var app = require('../src/app')
var debug = require('debug')('demo:server')
// var https = require('https')
var http = require('http')
var fs = require('fs')

// var options = {
//     key: fs.readFileSync('./ssl/ssl.key'),
//     cert: fs.readFileSync('./ssl/ssl.pem')
// }

var port = 3000
/**
 * Create HTTP/HTTPS server
 */
var server = http.createServer(app.callback())
// var server = https.createServer(options ,app.callback())

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, (res) => {
  console.log(`listening on ${port}`)
})
server.on('error', onError)
server.on('listening', onListening)

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges')
      process.exit(1)
    case 'EADDRINUSE':
      console.error(bind + ' is already in use')
      process.exit(1)
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address()
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  debug('Listening on ' + bind)
}
