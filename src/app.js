const Koa = require('koa')
const json = require('koa-json')
const KoaRouter = require('koa-router')
const path = require('path')
const render = require('koa-ejs')
const cors = require('koa2-cors')
const KoaBody = require('koa-body')
const bodyParser = require('koa-bodyparser')
const sslify = require('koa-sslify').default
const seq = require('./db/seq')

// 路由引入
const homeRouter = require('./routes/api/home') // 首页，左侧的菜单数据
const zhaopinRouter = require('./routes/api/zhaopin')
const zhaopinstateRouter = require('./routes/api/zhaopinstate')
// 员工相关路由处理
const yuangongRouter = require('./routes/api/yuangong')
const categoryRouter = require('./routes/api/category')
const gangweiRouter = require('./routes/api/gangwei')
const workaddrRouter = require('./routes/api/workaddr')
const gradeRouter = require('./routes/api/grade')
const qiniuRouter = require('./routes/api/qiniu')
// 员工技能相关
const skillRouter = require('./routes/api/skill/skill')
const shulianRouter = require('./routes/api/skill/shulian')
const ygskillRouter = require('./routes/api/skill/ygskill')
// 组织架构管理
const bumenRouter = require('./routes/api/jiagou/bumen')

const app = new Koa()
const router = new KoaRouter()

app.use(json())
app.use(cors())
app.use(KoaBody())
app.use(bodyParser())
// app.use(sslify())

// 路由处理
// router.get('/')
app.use(homeRouter.routes(), homeRouter.allowedMethods())
app.use(zhaopinRouter.routes(), zhaopinRouter.allowedMethods())
app.use(zhaopinstateRouter.routes(), zhaopinstateRouter.allowedMethods())
app.use(yuangongRouter.routes(), yuangongRouter.allowedMethods())
app.use(categoryRouter.routes(), categoryRouter.allowedMethods())
app.use(gangweiRouter.routes(), gangweiRouter.allowedMethods())
app.use(workaddrRouter.routes(), workaddrRouter.allowedMethods())
app.use(gradeRouter.routes(), gradeRouter.allowedMethods())
app.use(qiniuRouter.routes(), qiniuRouter.allowedMethods())
app.use(skillRouter.routes(), skillRouter.allowedMethods())
app.use(shulianRouter.routes(), shulianRouter.allowedMethods())
app.use(ygskillRouter.routes(), ygskillRouter.allowedMethods())
app.use(bumenRouter.routes(), bumenRouter.allowedMethods())
// 测试连接
seq
  .authenticate()
  .then(() => {
    console.log('数据库连接成功')
  })
  .catch(() => {
    console.log('数据库连接失败')
  })

module.exports = app
