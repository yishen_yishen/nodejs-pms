/**
 * @description 时间相关的工具函数、公共方法
 * @author yishen@mdashen.com
 */
// 时间格式化方法
const { ZhaopinState } = require('../db/model/index')
Date.prototype.format = function (format) {
  var date = {
    'M+': this.getMonth() + 1,
    'd+': this.getDate(),
    'h+': this.getHours(),
    'm+': this.getMinutes(),
    's+': this.getSeconds(),
    'q+': Math.floor((this.getMonth() + 3) / 3),
    S: this.getMilliseconds(),
  }
  if (/(y+)/i.test(format)) {
    format = format.replace(
      RegExp.$1,
      (this.getFullYear() + '').substr(4 - RegExp.$1.length)
    )
  }
  for (var k in date) {
    if (new RegExp('(' + k + ')').test(format)) {
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length === 1
          ? date[k]
          : ('00' + date[k]).substr(('' + date[k]).length)
      )
    }
  }
  return format
}

// TODO 写一个将出错信息，log的方法，传入error对象，相关说明，请求api
// 向下面这样
// try {
// } catch (error) {
//   console.log('修改员工技能熟练度出错')
//   console.log(error)
// }

// 发布完招聘信息后，新增此条招聘记录的招聘状态
async function zpstateAdd(zpid) {
  try {
    var res = await ZhaopinState.create({
      zpid,
    })
  } catch (error) {
    console.log(error)
    return {
      error_code: 1,
      msg: '添加失败，可能原因：传入zpid不存在，或此zpid以设置state',
    }
  }
  if (res) {
    return {
      error_code: 0,
      msg: '添加成功',
      data: res,
    }
  }
}

module.exports = {
  zpstateAdd
}
