/**
 * @description 招聘管理页面的路由处理
 * @author yishen@mdashen.com
 *
 */
const { Op } = require('sequelize')
const router = require('koa-router')()
const { Zhaopin, Zpstate } = require('../../db/model/index')
const { zpstateAdd } = require('../../utils/utils')

// 获取招聘信息(所有字段)，支持分页，仅支持部门和岗位两个字段的模糊查询
// TODO 优化为不返回所有字段，只返回前端展示需要的字段,然后加一个获取单条招聘信息全部字段的接口
router.get('/zp/getList', async (ctx, next) => {
  const { query, pagenum, pagesize } = ctx.request.query
  if (pagenum - 0 < 1) {
    ctx.body = { error_code: 1, msg: 'pagenum传入错误' }
    return
  }
  const result = await Zhaopin.findAndCountAll({
    attributes: [
      'id',
      'bumen',
      'data',
      'gangwei',
      'num',
      'shiyou',
      'daogangdata',
      'xueli',
      'zhuanye',
      'gender',
      'age',
      'salary',
      'experience',
      'skill',
      'zhize',
      'remark',
    ],
    where: {
      [Op.or]: [
        {
          bumen: {
            [Op.like]: `%${query}%`,
          },
        },
        {
          gangwei: {
            [Op.like]: `%${query}%`,
          },
        },
      ],
    },
    order: [['data', 'DESC']],
    limit: pagesize - 0,
    offset: (pagenum - 1) * pagesize,
  })
  let zhaopinList = result.rows.map((row) => row.dataValues)
  ctx.body = {
    error_code: 0,
    msg: '获取成功！',
    count: result.count,
    data: zhaopinList,
  }
})
// 传入zpid，获取此条招聘的详细信息
router.get('/zp/queryone', async (ctx, next) => {
  const { zpid } = ctx.request.query
  const res = await Zhaopin.findOne({
    attributes: [
      'id',
      'bumen',
      'data',
      'gangwei',
      'num',
      'shiyou',
      'daogangdata',
      'xueli',
      'zhuanye',
      'gender',
      'age',
      'salary',
      'experience',
      'skill',
      'zhize',
      'remark',
    ],
    where: { id: zpid },
  })
  if (res === null) {
    ctx.body = {
      error_code: 0,
      msg: '获取失败，没有该条招聘信息，请检查传入zpid是否正确',
      data: zpid,
    }
  } else {
    ctx.body = {
      error_code: 0,
      msg: '获取成功',
      data: res,
    }
  }
})
// 根据id删除招聘信息(真删除，非逻辑删除)
router.post('/zp/del', async (ctx, next) => {
  const { zpid } = ctx.request.body
  const result = await Zhaopin.destroy({
    where: {
      id: zpid,
    },
  })
  if (result === 1) {
    ctx.body = { error_code: 0, msg: `删除成功、数据库受影响:${result}行` }
  } else {
    ctx.body = { error_code: 1, msg: '发生错误，请检查掺入的zpid是否错误' }
  }
})
// 新增招聘记录，可以用了。处理一下res就行了
router.post('/zp/add', async (ctx, next) => {
  const {
    bumen,
    data,
    gangwei,
    num,
    shiyou,
    daogangdata,
    xueli,
    zhuanye,
    gender,
    age,
    salary,
    experience,
    skill,
    zhize,
    remark,
  } = ctx.request.body
  const res = await Zhaopin.create({
    bumen,
    data,
    gangwei,
    num,
    shiyou,
    daogangdata,
    xueli,
    zhuanye,
    gender,
    age,
    salary,
    experience,
    skill,
    zhize,
    remark,
  })
  // 在这里同步新增招聘状态 zpid:res.id
  await zpstateAdd(res.id)
  ctx.body = {
    error_code: 0,
    msg: '添加成功',
    data: res,
  }
})
// 修改招聘信息，待实现：不能修改部门名称、招聘岗位、发布时间
router.post('/zp/update', async (ctx, next) => {
  const {
    id,
    bumen,
    data,
    gangwei,
    num,
    shiyou,
    daogangdata,
    xueli,
    zhuanye,
    gender,
    age,
    salary,
    experience,
    skill,
    zhize,
    remark,
  } = ctx.request.body
  const res = await Zhaopin.update(
    {
      bumen,
      data,
      gangwei,
      num,
      shiyou,
      daogangdata,
      xueli,
      zhuanye,
      gender,
      age,
      salary,
      experience,
      skill,
      zhize,
      remark,
    },
    {
      where: {
        id,
      },
    }
  )
  console.log(res)
  const res_select = await Zhaopin.findOne({ where: { id } })
  if (res[0] === 1) {
    ctx.body = { error_code: 0, msg: '修改成功', data: res_select }
  } else {
    ctx.body = {
      error_code: 1,
      msg: '修改错误，请检查id是传入错误',
      data: res_select,
    }
  }
})
module.exports = router
