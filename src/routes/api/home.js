// home页面所需要的接口数据
const menuList = [
  {
    id: 1,
    path: 'zhaoping',
    menuName: '招聘管理',
    icon: 'iconfont iconjianli',
    subMenu: [
      {
        id: 11,
        path: 'zhaoping-xuqiu',
        menuName: '招聘需求管理',
        icon: 'iconfont iconxuqiu-copy',
      },
      {
        id: 12,
        path: 'zhaoping-banli',
        menuName: '办理',
        icon: 'iconfont iconbanli',
      },
    ],
  },
  {
    id: 2,
    path: 'yuangong',
    menuName: '员工管理',
    icon: 'iconfont iconyuangong',
    subMenu: [
      {
        id: 21,
        path: 'yuangong-xinxi',
        menuName: '员工信息管理',
        icon: 'iconfont iconyuangongxinxi',
      },
      {
        id: 22,
        path: 'yuangong-zhuangtai',
        menuName: '员工类型、状态',
        icon: 'iconfont iconzhuangtai',
      },
    ],
  },
  {
    id: 3,
    path: 'jineng',
    menuName: '员工技能管理',
    icon: 'iconfont iconjineng',
    subMenu: [
      {
        id: 31,
        path: 'jineng-liebiao',
        menuName: '技能列表',
        icon: 'iconfont iconliebiao',
      },
      {
        id: 32,
        path: 'jineng-shulian',
        menuName: '技能熟练度',
        icon: 'iconfont iconbili',
      },
      {
        id: 33,
        path: 'jineng-fenzhi',
        menuName: '技能分管理',
        icon: 'iconfont iconguanlipeizhi',
      },
    ],
  },
  // {
  //   id: 4,
  //   path: 'jiagou',
  //   menuName: '组织架构管理',
  //   icon: 'iconfont iconzuzhi',
  //   subMenu: [
  //     {
  //       id: 41,
  //       path: 'jiagou-bumen',
  //       menuName: '部门',
  //       icon: 'iconfont iconmorenbumen',
  //     },
  //     {
  //       id: 42,
  //       path: 'jiagou-xiangmu',
  //       menuName: '项目',
  //       icon: 'iconfont iconicon-project',
  //     },
  //     {
  //       id: 43,
  //       path: 'jiagou-yuangong',
  //       menuName: '员工',
  //       icon: 'iconfont iconyuangongguanli',
  //     },
  //   ],
  // },
]

const router = require('koa-router')()
router.get('/home/menus', async (ctx, next) => {
  ctx.body = { error_code: 0, msg: '获取成功', data: menuList }
})
module.exports = router
