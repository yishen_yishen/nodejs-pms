/**
 * @description 员工类别相关的路由处理
 * @author yishen@mdashen.com
 */
const router = require('koa-router')()
const { Category, Gangwei } = require('../../db/model/index')

// 获取分类列表
router.get('/cg/getlist', async (ctx, next) => {
  const res = await Category.findAndCountAll({
    attributes: ['id', 'name'],
  })
  const categoryList = res.rows.map((row) => row.dataValues)
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    count: res.count,
    data: categoryList,
  }
})
// 传入类别名称 name,添加新的类别
router.post('/cg/add', async (ctx, next) => {
  const { name } = ctx.request.body
  try {
    var res = await Category.create({ name })
  } catch (error) {
    console.log('错误,重复添加类别')
    console.log(error)
    ctx.body = {
      error_code: 1,
      msg: '添加失败，已有该类别，请勿重复添加',
    }
    return
  }
  ctx.body = {
    error_code: 0,
    msg: '添加成功',
    data: res,
  }
})
// 传入cgid(分类id),和force(强制删除)字段，删除分类(以及分类下的岗位)
router.post('/cg/del', async (ctx, next) => {
  let { cgid } = ctx.request.body
  cgid = cgid - 0
  const { name, force } = ctx.request.body
  if (name && name.length > 0) {
    // 传名字，要先自查cgid
    const res = await Category.findOne({
      attributes: ['id', 'name'],
      where: { name },
    })
    if (res) {
      cgid = res.dataValues.id
    } else {
      ctx.body = { error_code: 1, msg: 'name传入错误，无此分类' }
      return
    }
  }
  // 非强制删除，要判断该分类下是否有岗位存在
  if (force !== 'force') {
    const res = await Gangwei.findAndCountAll({
      where: {
        cgid,
      },
    })
    const count = res.count
    if (count > 0) {
      // 分类下存在岗位无法删除
      ctx.body = {
        error_code: 1,
        msg: `该分类下存在${count}条岗位信息，无法删除`,
      }
      return
    } else {
      // 此分类下没有岗位，可以删除
      ctx.body = await delCategory(cgid)
    }
    return
  }
  ctx.body = await delCategory(cgid)
})

/**
 * 删除分类(id)，以及分类下的岗位(外键限制)
 * @param {object} where 查找条件对象
 * @returns ctx.body对象
 */
async function delCategory(cgid) {
  try {
    var result = await Category.destroy({
      where: { id: cgid },
    })
  } catch (error) {
    console.log('报错了')
  }
  if (result === 1) {
    return { error_code: 0, msg: `删除成功、本数据表受影响:${result}行` }
  } else {
    return { error_code: 1, msg: '发生错误，请检查传入的cgid或者name是否错误' }
  }
}

module.exports = router
