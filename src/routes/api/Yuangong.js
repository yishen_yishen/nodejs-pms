/**
 * @description 员工管理相关的路由处理
 * @author yishen@mdashen.com
 */
// BUG 时间格式化用的，这么引入好像不规范吧！
require('../../utils/utils')
const { Op } = require('sequelize')
const router = require('koa-router')()
const { Yuangong } = require('../../db/model/index')
// 获取员工信息(非返回所有字段),支持分页。
router.get('/yg/getlist', async (ctx, next) => {
  const { query, pagenum, pagesize } = ctx.request.query
  if (pagenum - 0 < 1) {
    ctx.body = { error_code: 1, msg: 'pagenum传入错误' }
    return
  }
  const res = await Yuangong.findAndCountAll({
    //   不用返回所有字段，在下一个通过id或者姓名返回该员工详细信息的接口
    attributes: ['id', 'name', 'category', 'grade', 'enterdata', 'updatedAt'],
    where: {
      [Op.or]: [
        {
          name: {
            [Op.like]: `%${query}%`,
          },
        },
        {
          category: {
            [Op.like]: `%${query}%`,
          },
        },
      ],
    },
    order: [['updatedAt', 'DESC']],
    limit: pagesize - 0,
    offset: (pagenum - 1) * pagesize,
  })
  const YuangongList = res.rows.map((row) => row.dataValues)
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    count: res.count,
    data: YuangongList,
  }
})
// 获取全部员工的id、姓名列表
router.get('/yg/getnamelist', async (ctx, next) => {
  const res = await Yuangong.findAndCountAll({
    attributes: ['id', 'name'],
  })
  const list = res.rows.map((row) => row.dataValues)
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    data: list,
  }
})
// 通过员工id，获取关于该员工的所有信息
router.get('/yg/queryone', async (ctx, next) => {
  const { ygid } = ctx.request.query
  const res = await Yuangong.findOne({
    attributes: [
      'id',
      'name',
      'gender',
      'age',
      'tel',
      'avatar',
      'idcard',
      'jiguan',
      'address',
      'school',
      'xueli',
      'enterdata',
      'outdata',
      'bumen',
      'salary',
      'gangwei',
      'category',
      'grade',
      'workaddr',
      'updatedAt',
    ],
    where: { id: ygid },
  })
  if (res === null) {
    ctx.body = {
      error_code: 0,
      msg: '获取失败，没有该员工，请检查传入id是否正确',
      data: id,
    }
  } else {
    ctx.body = {
      error_code: 0,
      msg: '获取成功',
      data: res,
    }
  }
})
router.post('/yg/del', async (ctx, next) => {
  const { ygid } = ctx.request.body
  const result = await Yuangong.destroy({
    where: {
      id: ygid,
    },
  })
  if (result === 1) {
    ctx.body = { error_code: 0, msg: `删除成功、数据库受影响:${result}行` }
  } else {
    ctx.body = { error_code: 1, msg: '发生错误，请检查掺入的ygid是否错误' }
  }
})
router.post('/yg/update', async (ctx, next) => {
  const {
    id,
    name,
    gender,
    age,
    avatar,
    idcard,
    jiguan,
    address,
    school,
    tel,
    salary,
    xueli,
    enterdata,
    bumen,
    gangwei,
    category,
    grade,
    workaddr,
  } = ctx.request.body
  var { outdata } = ctx.request.body
  // 离职时间传入为空，修改为一个特定值
  if (!outdata) {
    outdata = '1999-1-1'
  }
  const res = await Yuangong.update(
    {
      name,
      gender,
      age,
      avatar,
      idcard,
      jiguan,
      address,
      school,
      xueli,
      enterdata,
      outdata,
      bumen,
      gangwei,
      category,
      grade,
      workaddr,
      tel,
      salary,
    },
    {
      where: {
        id,
      },
    }
  )
  const res_select = await Yuangong.findOne({ where: { id } })
  if (res[0] === 1) {
    ctx.body = { error_code: 0, msg: '员工修改成功', data: res_select }
  } else {
    ctx.body = {
      error_code: 1,
      msg: '修改错误，请检查id是传入错误',
      data: res_select,
    }
  }
})
router.post('/yg/add', async (ctx, next) => {
  const {
    name,
    gender,
    age,
    idcard,
    jiguan,
    address,
    school,
    xueli,
    bumen,
    gangwei,
    category,
    grade,
    workaddr,
    tel,
    salary,
  } = ctx.request.body
  const enterdata = new Date().format('YYYY-MM-dd')
  console.log(enterdata)
  // TODO 传入的身份证号码，应该和已有的身份证比较，不能重复
  const res = await Yuangong.create({
    name,
    gender,
    age,
    idcard,
    jiguan,
    address,
    school,
    xueli,
    enterdata,
    bumen,
    gangwei,
    category,
    grade,
    workaddr,
    tel,
    salary,
  })
  ctx.body = {
    error_code: 0,
    msg: '添加成功',
    data: res,
  }
})
module.exports = router
