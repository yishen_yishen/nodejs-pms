/**
 * @description 招聘状态管理相关的路由处理
 * @author yishen@mdashen.com
 */
const router = require('koa-router')()
// ASK 作连表查询，要引入index文件(外键关联定义到了index里面)
// 我之前是分别引入两个模型，怪不得一直错误，哈哈哈
const { ZhaopinState, Zhaopin } = require('../../db/model/index')
const { zpstateAdd } = require('../../utils/utils')

// TODO 没有设置删除接口
// 数据库中做了外键限制，当zp信息被删除时，zp信息对应的状态也会被自动删除

// 新增招聘状态，传入招聘id，默认将招聘状态设为0(已启动)
router.post('/zpstate/add', async (ctx, next) => {
  let { zpid } = ctx.request.body
  zpid = zpid - 0
  const res = await zpstateAdd(zpid)
  ctx.body = res
})
// 修改招聘状态，传入zpid(即zhaopin表中的id字段/zpstate表中的zpid字段)，和修改后的值(0,1,2)
router.post('/zpstate/update', async (ctx, next) => {
  let { zpid, state } = ctx.request.body
  zpid = zpid - 0
  state = state - 0
  // TODO 定义模型时候，也加上验证
  if (state !== 0 && state !== 1 && state !== 2) {
    ctx.body = {
      error_code: 1,
      msg: '修改失败，state不符合要求(0,1,2)',
    }
    return
  }
  const res = await ZhaopinState.update({ state }, { where: { zpid } })
  if (res[0] === 1) {
    ctx.body = { error_code: 0, msg: '修改成功' }
  } else {
    ctx.body = {
      error_code: 1,
      msg: '修改失败，请检测传入zpid是否存在',
    }
  }
})
// 通过招聘ID获取该条招聘信息的状态，传入zpid
router.get('/zpstate/querybyzpid', async (ctx, next) => {
  let { zpid } = ctx.request.query
  zpid = zpid - 0
  const res = await ZhaopinState.findOne({
    attributes: ['zpid', 'state'],
    where: { zpid },
    include: [{ model: Zhaopin, attributes: ['bumen', 'gangwei', 'data'] }],
  })
  // TODO 返回的数据中，没做扁平化处理(存在对象中套对象)
  if (!res) {
    ctx.body = {
      error_code: 1,
      msg: '获取失败，传入的zpid不存在',
    }
    return
  }
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    data: res,
  }
})
// 通过传入状态值，获取该状态值下有哪些招聘信息
router.get('/zpstate/querybystate', async (ctx, next) => {
  let { state } = ctx.request.query
  state = state - 0
  if (state !== 0 && state !== 1 && state !== 2) {
    ctx.body = {
      error_code: 1,
      msg: '获取失败，state不符合要求(0,1,2)',
    }
    return
  }
  const res = await ZhaopinState.findAndCountAll({
    attributes: ['zpid', 'state'],
    where: { state },
    include: [{ model: Zhaopin, attributes: ['bumen', 'gangwei', 'data'] }],
  })
  // 扁平化数据处理
  let zpList = res.rows.map((row) => row.dataValues)
  zpList.map((item) => {
    item.bumen = item.zhaopin.bumen
    item.gangwei = item.zhaopin.gangwei
    item.data = item.zhaopin.data
    delete item.zhaopin
  })
  console.log(zpList)
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    count: res.count,
    data: zpList,
  }
})

// 数据库中直接导入zp信息的话，zpstate是不会同步增加的
// 前台有同步按钮(设置为隐藏)
module.exports = router
