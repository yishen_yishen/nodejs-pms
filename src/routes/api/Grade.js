/**
 * @description 员工定级管理相关的路由处理
 * @author yishen@mdashen.com
 *
 */
const router = require('koa-router')()
const Grade = require('../../db/model/Grade')
// 获取所有定级
router.get('/grade/getlist', async (ctx, next) => {
  const res = await Grade.findAndCountAll({
    attributes: ['id', 'name'],
  })
  const GradeList = res.rows.map((row) => row.dataValues)
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    count: res.count,
    data: GradeList,
  }
})
// 新增员工定级
router.post('/grade/add', async (ctx, next) => {
  const { name } = ctx.request.body
  try {
    var res = await Grade.create({ name })
  } catch (error) {
    console.log('错误,重复添加人员定级')
    console.log(error)
    ctx.body = {
      error_code: 1,
      msg: '添加失败，已有该人员定级，请勿重复添加',
    }
    return
  }
  ctx.body = {
    error_code: 0,
    msg: '添加成功',
    data: res,
  }
})
// 删除员工定级
router.post('/grade/del', async (ctx, next) => {
  const { id, name } = ctx.request.body
  // 通过id删除
  if (id && id.length > 0) {
    const res = await Grade.destroy({
      where: {
        id,
      },
    })
    if (res === 1) {
      ctx.body = { error_code: 0, msg: `删除成功、数据库受影响:${res}行` }
      return
    } else {
      ctx.body = { error_code: 1, msg: '发生错误，请检查传入的id是否错误' }
      return
    }
  }
  // 通过name删除
  if (name && name.length > 0) {
    const res = await Grade.destroy({
      where: {
        name,
      },
    })
    if (res === 1) {
      ctx.body = { error_code: 0, msg: `删除成功、数据库受影响:${res}行` }
      return
    } else {
      ctx.body = {
        error_code: 1,
        msg: '发生错误，请检查传入的人员定级名称是否错误',
      }
      return
    }
  }
})
module.exports = router
