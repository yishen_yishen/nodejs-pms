/**
 * @description 部门相关的路由处理
 * @author yishen@mdashen.com
 */
const router = require('koa-router')()
const { Bumen } = require('../../../db/model/index')

// 获取所有部门信息
router.get('/bumen/getall', async (ctx, next) => {
  const res = await Bumen.findAll({
    attributes: ['id', 'bumen', 'level', 'parent'],
  })
  // TODO 数据处理，变成children那样的
  const level0 = []
  let level1 = []
  const level2 = []
  res.map((item) => {
    // 最高允许三级
    if (item.level === 0) {
      level0.push(item)
    }
    if (item.level === 1) {
      level1.push(item)
    }
    if (item.level === 2) {
      level2.push(item)
    }
  })
  // ASK 我也不想用双层循环，没想到好办法啊啊啊啊啊啊啊啊！
  level1.map((item) => {
    item.dataValues.children = []
    level2.map((item1, index) => {
      if (item1.parent === item.id) {
        item.dataValues.children.push(item1)
      }
    })
  })
  level0.map((item) => {
    item.dataValues.children = []
    level1.map((item1, index) => {
      if (item1.parent === item.id) {
        item.dataValues.children.push(item1)
      }
    })
  })
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    data: level0,
  }
})
// 新增部门
router.post('/bumen/add', async (ctx, next) => {
  const { bumen, level, parent } = ctx.request.body
  if (!bumen || !level || !parent) {
    ctx.body = {
      error_code: 1,
      msg: '缺少参数',
    }
    return
  }
  // 二级部门的父亲不能为0
  if (level > 0 && parent < 1) {
    ctx.body = {
      error_code: 1,
      msg: '二级及以上部门 的 父部门id 不能为0',
    }
    return
  }
  // 最大允许三级 0 1 2
  if (level > 2) {
    ctx.body = {
      error_code: 1,
      msg: '最多允许三级',
    }
    return
  }
  // 查询parent对应的id是否存在，不存在返回错误
  if (parent - 0 !== 0) {
    const res = await Bumen.findOne({ where: { id: parent } })
    // parent===0代表根部门
    if (res === null) {
      ctx.body = {
        error_code: 1,
        msg: 'parent字段错误，此id对应部门不存在',
      }
      return
    }
  }
  try {
    var data = await Bumen.create({ bumen, level, parent })
  } catch (error) {
    console.log('部门添加失败!')
    console.log(error)
    ctx.body = {
      error_code: 1,
      msg: '部门添加失败',
    }
    return
  }
  ctx.body = {
    error_code: 0,
    msg: '新增成功',
  }
})
// 删除部门
router.post('/bumen/del', async (ctx, next) => {
  // 删除部门，同时要删除部门下的子部门
  const { id } = ctx.request.body
  const res = await Bumen.destroy({
    where: { id },
  })
  if (res !== 1) {
    ctx.body = {
      error_code: 1,
      msg: '删除失败，id传入错误',
    }
    return
  }
  // 同时删除，此部门的所有子部门
  await Bumen.destroy({
    where: { parent: id },
  })
  ctx.body = {
    error_code: 0,
    msg: '删除成功',
  }
})

module.exports = router
