/**
 * @description 部门相关的路由处理
 * @author yishen@mdashen.com
 */
const router = require('koa-router')()
const { Xiangmu } = require('../../../db/model/index')

module.exports = router
