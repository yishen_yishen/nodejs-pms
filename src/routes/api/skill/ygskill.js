/**
 * @description 技能熟练度的路由处理
 * @author yishen@mdashen.com
 */
const { Op, where } = require('sequelize')
const router = require('koa-router')()
const { YgSkill, Shulian, Skill, Yuangong } = require('../../../db/model/index')

// 获取所有员工的技能情况(技能名称、熟练度、分值、总分值),连表查询
// TODO 实现 query参数，想要支持query需要查询另一个表，用另外一个表的查询结果，来展示此表
// 有点复杂，不会搞
router.get('/ygskill/getlist', async (ctx, next) => {
  let { query, skid, than, slid } = ctx.request.query
  let where = {}
  if (!skid || !than || !slid) {
    // 三个参数有一个及以上没传(传参不全),不进行查询
    where = {}
  } else {
    skid = skid - 0
    than = than - 0
    slid = slid - 0
  }
  if (than === 1) {
    // 大于
    where = { [Op.and]: [{ skillid: skid }, { slid: { [Op.gt]: slid } }] }
  }
  if (than === 0) {
    // 小于
    where = { [Op.and]: [{ skillid: skid }, { slid: { [Op.lt]: slid } }] }
  }
  const res = await YgSkill.findAndCountAll({
    attributes: ['ygid'],
    where,
  })
  const _ = res.rows.map((row) => row.dataValues)
  // 过滤重复的
  const idlist = []
  _.map((item) => {
    idlist.indexOf(item.ygid) === -1 ? idlist.push(item.ygid) : ''
  })
  const ygsklist = []
  for (let i = 0; i < idlist.length; i++) {
    const id = idlist[i]
    const res = await getOneById(id)
    ygsklist.push(res)
  }
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    count: ygsklist.length,
    data: ygsklist,
  }
})
// 获取特定员工的所有技能情况(技能名称、熟练度、分值、总分值)
router.get('/ygskill/getone', async (ctx, next) => {
  const { ygid } = ctx.request.query
  try {
    var ygskillList = await getOneById(ygid)
  } catch (error) {
    console.log('/ygskill/getone，发生错误')
    console.log(error)
    ctx.body = { error_code: 1, msg: '获取失败，请检查传入ygid是否正确' }
    return
  }
  ctx.body = { error_code: 0, msg: '获取成功', data: ygskillList }
})
// 员工新增技能，默认新增为不熟练，后面通过修改，来提升熟练度
router.post('/ygskill/add', async (ctx, next) => {
  const { ygid, skid } = ctx.request.body
  // 熟练度id，默认新增为不熟练
  const slid = 1
  try {
    await YgSkill.create({ ygid, skillid: skid, slid })
  } catch (error) {
    console.log('新增员工技能，发生错误')
    console.log(error)
    ctx.body = { error_code: 1, msg: '添加失败，请检查传入数据.' }
    return
  }
  ctx.body = { error_code: 0, msg: '添加成功' }
})
// 修改员工技能熟练度
router.post('/ygskill/update', async (ctx, next) => {
  const { ygid, skid, slid } = ctx.request.body
  if (!ygid || !skid || !skid) {
    ctx.body = { error_code: 1, msg: '缺少参数' }
    return
  }
  try {
    var res = await YgSkill.update({ slid }, { where: { ygid, skillid: skid } })
  } catch (error) {
    console.log('修改员工技能熟练度出错')
    console.log(error)
    ctx.body = { error_code: 1, msg: '修改失败，请检查传入参数!' }
    return
  }
  if (res && res[0] === 1) {
    ctx.body = { error_code: 0, msg: '修改成功' }
    return
  } else {
    ctx.body = { error_code: 1, msg: '修改失败，请检查传入参数!' }
    return
  }
})

// TODO 考虑后期要不要加一个，新建员工后，把员工的所有技能(技能列表中所有值)默认设置为不了解
/**
 * @description 传入ygid，返回该员工的技能情况(对象)
 * @param {number} ygid 员工id
 */
async function getOneById(ygid) {
  const res = await YgSkill.findAndCountAll({
    attributes: ['ygid', 'skillid', 'slid'],
    include: [
      { model: Yuangong, attributes: ['name', 'bumen', 'gangwei'] },
      { model: Skill, attributes: ['name'] },
      { model: Shulian, attributes: ['shulian', 'fenzhi'] },
    ],
    where: { ygid },
  })
  const ygskillList = res.rows.map((row) => row.dataValues)
  //  数据扁平化处理,新郑total字段(总分)
  ygskillList.map((item) => {
    item.ygid = item.ygid
    item.name = item.yuangong.name
    item.bumen = item.yuangong.bumen
    item.gangwei = item.yuangong.gangwei
    item.total = 0
    item.sklist = [
      {
        skillid: item.skillid,
        skillname: item.skill.name,
        slid: item.slid,
        slname: item.skill_shulian.shulian,
        fenzhi: item.skill_shulian.fenzhi,
      },
    ]
    delete item.skillid
    delete item.slid
    delete item.skill
    delete item.yuangong
    delete item.skill_shulian
  })
  // 在遍历一次，把同一个人的sklist合并，同时计算该员工的总技能分
  let newygskillList = {}
  ygskillList.map((item, index) => {
    if (index === 0) {
      newygskillList = item
    } else {
      newygskillList.sklist.push(item.sklist[0])
    }
    newygskillList.total += item.sklist[0].fenzhi
  })
  return newygskillList
}

module.exports = router
