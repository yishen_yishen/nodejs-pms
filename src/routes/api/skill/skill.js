/**
 * @description 技能名称相关路由处理
 * @author yishen@mdashen.com
 */
const router = require('koa-router')()
const { Skill } = require('../../../db/model/index')

// 获取技能列表
router.get('/skill/getlist', async (ctx, next) => {
  const res = await Skill.findAndCountAll({
    attributes: ['id', 'name'],
  })
  const skillList = res.rows.map((row) => row.dataValues)
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    count: res.count,
    data: skillList,
  }
})
// 新增技能
router.post('/skill/add', async (ctx, next) => {
  // 新增技能，不区分大小写的，例如php和PHP是同一个东西
  const { name } = ctx.request.body
  try {
    var res = await Skill.create({ name })
  } catch (error) {
    console.log('错误,重复技能名称')
    console.log(error)
    ctx.body = {
      error_code: 1,
      msg: '添加失败，已有该技能，请勿重复添加',
    }
    return
  }
  ctx.body = {
    error_code: 0,
    msg: '添加成功',
    data: res,
  }
})
// 删除技能
router.post('/skill/del', async (ctx, next) => {
  const { id, name } = ctx.request.body
  let where = {}
  // 通过name删除
  if (name && name.length > 0) {
    where = { name }
  }
  // 通过id删除
  if (id && id.length > 0) {
    where = { id }
  }
  const res = await Skill.destroy({
    where,
  })
  if (res === 1) {
    ctx.body = { error_code: 0, msg: `删除成功、数据库受影响:${res}行` }
    return
  } else {
    ctx.body = { error_code: 1, msg: '发生错误，请检查传入数据是否错误' }
    return
  }
})
module.exports = router
