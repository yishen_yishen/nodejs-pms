/**
 * @description 员工工作地点管理相关的路由处理
 * @author yishen@mdashen.com
 *
 */
const router = require('koa-router')()
const Workaddr = require('../../db/model/Workaddr')

// wa = workaddr，获取所有工作地点
router.get('/wa/getlist', async (ctx, next) => {
  const res = await Workaddr.findAndCountAll({
    attributes: ['id', 'name'],
  })
  const WorkaddrList = res.rows.map((row) => row.dataValues)
  ctx.body = {
    error_code: 0,
    msg: '获取成功',
    count: res.count,
    data: WorkaddrList,
  }
})
// 新增工作地点
router.post('/wa/add', async (ctx, next) => {
  const { name } = ctx.request.body
  try {
    var res = await Workaddr.create({ name })
  } catch (error) {
    console.log('错误,重复添加工作地点')
    console.log(error)
    ctx.body = {
      error_code: 1,
      msg: '添加失败，已有该工作地点，请勿重复添加',
    }
    return
  }
  ctx.body = {
    error_code: 0,
    msg: '添加成功',
    data: res,
  }
})
// 删除工作地点
router.post('/wa/del', async (ctx, next) => {
  const { id, name } = ctx.request.body
  let where = {}
  // 通过name删除
  if (name && name.length > 0) {
    where = { name }
  }
  // 通过id删除
  if (id && id.length > 0) {
    where = { id }
  }
  const res = await Workaddr.destroy({
    where,
  })
  if (res === 1) {
    ctx.body = { error_code: 0, msg: `删除成功、数据库受影响:${res}行` }
    return
  } else {
    ctx.body = { error_code: 1, msg: '发生错误，请检查传入数据是否错误' }
    return
  }
})
module.exports = router
