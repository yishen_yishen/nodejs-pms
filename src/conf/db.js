/**
 * @description 存储配置
 * @author yishen@mdashen.com
 */

let MYSQL_CONF = {
  host: 'localhost',
  user: 'root',
  password: '',
  port: '3306',
  database: 'pms',
}
// let MYSQL_CONF = {
//   host: '118.89.55.75',
//   user: 'api_mdashen.com',
//   password: 'api_mdashen_com',
//   port: '3306',
//   database: 'api_mdashen.com',
// }
module.exports = {
  MYSQL_CONF,
}
