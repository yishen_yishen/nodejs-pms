/**
 * @description 岗位名称 数据库模型，
 * @description 每个类别下，有若干岗位(一对多)，gangwei表中的岗位与类别对应。
 * @author yishen@mdashen.com
 */

const seq = require('../seq')
const DataTypes = require('sequelize')
const Gangwei = seq.define(
  'gangwei',
  {
    // cgid和category表中的id对应(外键限制，分类对岗位，一对多)
    cgid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '分类id，categoryid简写cgid',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '岗位名称',
    },
  },
  { tableName: 'gangwei', comment: '岗位名称表' }
)
module.exports = Gangwei
