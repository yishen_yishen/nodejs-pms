/**
 * @Author: yishen@mdashen.com
 * @Date: 2020-12-02 12:44:07
 * @LastEditTime: 2021-05-13 20:59:55
 * @Description: 员工信息 数据库模型
 */
// TODO 加验证 validate验证，很多表都没加
const seq = require('../seq')
const DataTypes = require('sequelize')
const Yuangong = seq.define(
  'yuangong',
  {
    name: { type: DataTypes.STRING, allowNull: false, comment: '姓名' },
    gender: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '性别,0为男,1为女',
    },
    age: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '年龄',
    },
    tel: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '手机号',
    },
    avatar: {
      type: DataTypes.STRING,
      comment: '头像地址',
    },
    idcard: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '身份证号码',
    },
    jiguan: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '籍贯',
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '家庭住址',
    },
    school: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '毕业院校',
    },
    xueli: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '学历',
    },
    enterdata: {
      type: 'TIMESTAMP',
      allowNull: false,
      comment: '入司时间',
    },
    salary: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '薪水',
    },
    outdata: {
      type: 'TIMESTAMP',
      comment: '离职时间',
    },
    bumen: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '部门名称',
    },
    // TODO 这些可配置的项目，可以做一个连表查询
    gangwei: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '岗位(可配置)',
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '类别(可配置)',
    },
    grade: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '人员定级(可配置)',
    },
    workaddr: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '办公地点(可配置)',
    },
  },
  { tableName: 'yuangong', comment: '员工信息表' }
)
module.exports = Yuangong
