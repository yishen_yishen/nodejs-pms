/**
 * @description 员工技能yuangongskill 存放员工的技能信息
 * @author yishen@mdashen.com
 */
const seq = require('../../seq')
const DataTypes = require('sequelize')
// ygid和skillid的unique表示，两个字段设置联合唯一索引
// 两个字段不同同时形同
const YgSkill = seq.define(
  'yg_skill',
  {
    ygid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: 'ygig_skillid',
      comment: '员工id(对应yuangong表中的id，一对一)',
    },
    // 员工id和技能id不能同时重复
    skillid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: 'ygig_skillid',
      comment: '技能id，对应skill表中的id，一对一',
    },
    slid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '熟练度id(shulianid)，对应shulian表中的id，一对一',
    },
  },
  {
    tableName: 'yg_skill',
    comment: '员工技能表，一个员工多个技能，就多条记录。',
  }
)
module.exports = YgSkill
