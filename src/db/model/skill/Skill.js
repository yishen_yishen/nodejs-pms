/**
 * @description 技能管理，记录所有技能名称
 * @author yishen@mdashen.com
 */
const seq = require('../../seq')
const DataTypes = require('sequelize')
const Skill = seq.define(
  'skill',
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notNull: true,
      },
      comment: '技能名称',
    },
  },
  {
    tableName: 'skill',
    comment: '技能列表',
  }
)
module.exports = Skill
