/**
 * @description 技能管理-熟练度数据表模型
 * @author yishen@mdashen.com
 */
const seq = require('../../seq')
const DataTypes = require('sequelize')
const Shulian = seq.define(
  'skill_shulian',
  {
    shulian: {
      // 类型用枚举可能会更好
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      // 只能是这些值中的一个,sequelize验证，非mysql约束
      validate: {
        isIn: [['不了解', '了解', '熟悉', '精通', '专家']],
      },
      comment:
        '技能管理的熟练度，熟练度名称(特定值)，不了解/了解/熟悉/精通/专家',
    },
    fenzhi: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
      comment: '每项技能熟练度对应的分值，如不了解对应0分...',
    },
  },
  {
    tableName: 'skill_shulian',
    comment: '技能熟练度',
  }
)
module.exports = Shulian
