/**
 * @description 员工类别 数据库模型，一个类别下有多种岗位，
 * @author yishen@mdashen.com
 */

const seq = require('../seq')
const DataTypes = require('sequelize')
const Category = seq.define(
  'category',
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '员工类别',
    },
  },
  { tableName: 'category', comment: '员工类别表' }
)
module.exports = Category
