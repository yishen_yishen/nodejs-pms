/**
 * @description 人员定级 数据库模型，
 * @author yishen@mdashen.com
 */

const seq = require('../seq')
const DataTypes = require('sequelize')
const Grade = seq.define(
  'grade',
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '人员定级',
    },
  },
  { tableName: 'grade', comment: '人员登记表' }
)
module.exports = Grade
