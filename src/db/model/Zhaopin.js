/**
 * @description 招聘需求 数据库模型
 * @author yishen@mdashen.com
 */
const seq = require('../seq')
const DataTypes = require('sequelize')
const Zhaopin = seq.define(
  'zhaopin',
  {
    bumen: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '部门名称',
    },
    data: {
      type: 'TIMESTAMP',
      defaultValue: seq.literal('CURRENT_TIMESTAMP'),
      comment: '登记表格的日期，默认当前时间',
    },
    gangwei: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '招聘岗位名称',
    },
    num: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '招聘人数',
    },
    shiyou: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '申请事由，新增岗位、后补、储备、其他',
    },
    daogangdata: {
      type: DataTypes.STRING,
      comment: '到岗时间',
    },
    // 资格需求部分
    xueli: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '学历',
    },
    zhuanye: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '专业',
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '性别',
    },
    age: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '年龄',
    },
    salary: {
      type: DataTypes.STRING,
      allowNull: false,
      comment: '拟聘薪资',
    },
    experience: {
      type: DataTypes.TEXT,
      comment: '工作经验要求',
    },
    skill: {
      type: DataTypes.TEXT,
      comment: '知识与技能要求',
    },
    zhize: {
      type: DataTypes.TEXT,
      comment: '招聘岗位工作职责',
    },
    remark: {
      type: DataTypes.TEXT,
      comment: '备注',
    },
  },
  {
    tableName: 'zhaopin',
    comment: '招聘信息表',
  }
)
module.exports = Zhaopin
