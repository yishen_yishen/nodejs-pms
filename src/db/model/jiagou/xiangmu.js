/**
 * @description 项目 数据库模型，一个部门下有多个项目，一个部门下有多个员工，
 * @author yishen@mdashen.com
 */

const seq = require('../../seq')
const DataTypes = require('sequelize')
const Xiangmu = seq.define(
  'jiagou_xiangmu',
  {
    xiangmu: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '项目名称',
    },
  },
  { tableName: 'jiagou_xiangmu', comment: '架构管理、项目表' }
)
module.exports = Xiangmu
