/**
 * @description 部门(多级树形) 数据库模型，一个部门下有多个项目，一个部门下有多个员工，
 * @author yishen@mdashen.com
 */

const seq = require('../../seq')
const DataTypes = require('sequelize')

// BUG parent应该和id关联
// TODO 部门放三个表，一级部门放一个表，二级部门放一个表，三级部门放一个表，这样就很容易关联了
// TODO 没找到什么关联方法，找到后面再补
const Bumen = seq.define(
  'jiagou_bumen',
  {
    bumen: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '部门名称',
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isNumeric: true,
        min: 0,
        max: 4,
      },
      comment: '层级，0级别为根级,最多允许4级',
    },
    parent: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isNumeric: true,
        min: 0,
      },
      comment: '父亲的id，根组件的父亲id为0',
    },
  },
  { tableName: 'jiagou_bumen', comment: '架构管理、部门表' }
)
module.exports = Bumen
