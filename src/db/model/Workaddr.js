/**
 * @description 岗位名称 数据库模型，
 * @author yishen@mdashen.com
 */
const seq = require('../seq')
const DataTypes = require('sequelize')
const Workaddr = seq.define(
  'workaddr',
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      comment: '工作地点',
    },
  },
  { tableName: 'workaddr', comment: '工作地点表' }
)
module.exports = Workaddr
