/**
 * @description 数据模型入口文件,定义相关外键限制
 * @author yishen@mdashen.com
 */
const Zhaopin = require('./Zhaopin')
const ZhaopinState = require('./ZhaopinState')
// 员工管理状态相关
const Yuangong = require('./Yuangong')
const Gangwei = require('./Gangwei')
const Grade = require('./Grade')
const Workaddr = require('./Workaddr')
const Category = require('./Category')
// 员工技能、技能分相关
const YgSkill = require('./skill/YgSkill')
const Skill = require('./skill/Skill')
const Shulian = require('./skill/Shulian')
// 组织结构管理
const Bumen = require('./jiagou/bumen')
const Xiangmu = require('./jiagou/xiangmu')

// 创建关联关系,zhaopin表和zhaopinstate表，一对一的关系(通过zhaopinstate表中的zpid连接起来)
Zhaopin.hasOne(ZhaopinState, {
  foreignKey: 'zpid',
})
ZhaopinState.belongsTo(Zhaopin, {
  foreignKey: 'zpid',
})
// 关联方式: category表和gangwei表，一对多关系
// 通过gangwei表中的cgid(categoryid)连接起来
Category.hasMany(Gangwei, {
  foreignKey: 'cgid',
})
Gangwei.belongsTo(Category, {
  foreignKey: 'cgid',
})
// 员工表和，员工技能表 一对多关系
Yuangong.hasMany(YgSkill, {
  foreignKey: 'ygid',
})
YgSkill.belongsTo(Yuangong, {
  foreignKey: 'ygid',
})
// TODO 下面这两个关联，有点乱，头大！
// 技能名称表，员工技能
Skill.hasOne(YgSkill, {
  foreignKey: 'skillid',
})
YgSkill.belongsTo(Skill, {
  foreignKey: 'skillid',
})
// 熟练度表、员工技能表
Shulian.hasOne(YgSkill, {
  foreignKey: 'slid',
})
YgSkill.belongsTo(Shulian, {
  foreignKey: 'slid',
})

module.exports = {
  Zhaopin,
  ZhaopinState,
  Yuangong,
  Gangwei,
  Grade,
  Workaddr,
  Category,
  YgSkill,
  Skill,
  Shulian,
  Bumen,
  Xiangmu,
}
