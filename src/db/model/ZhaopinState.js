/**
 * @description 招聘状态
 * @author yishen@mdashen.com
 */
const seq = require('../seq')
const DataTypes = require('sequelize')
const ZhaopinState = seq.define(
  'zhaopin_state',
  {
    //  zpid要和zhaopin表中的id一一对应(外键限制)
    zpid: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
      comment: '招聘信息的ID',
    },
    state: {
      type: DataTypes.INTEGER,
      allowNull: false,
      // 默认值为0，已启动
      defaultValue: 0,
      comment: '招聘信息的状态，已启动(0)、已完成(1)、已撤销(2)',
    },
  },
  {
    tableName: 'zhaopin_state',
    comment: '招聘信息状态表',
  }
)
module.exports = ZhaopinState
