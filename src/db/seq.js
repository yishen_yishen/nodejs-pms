/**
 * @description sequelize 实例
 * @author yishen@mdashen.com
 */

const Sequelize = require('sequelize')
const { MYSQL_CONF } = require('../conf/db')

const { host, user, password, database } = MYSQL_CONF
const conf = {
  host,
  dialect: 'mysql',
}
conf.pool = {
  max: 3, // 连接池中最大的连接数量
  min: 0, // 最小
  idle: 10000, // 如果一个连接池 10 s 之内没有被使用，则释放
}
const seq = new Sequelize(database, user, password, conf, {
  // timezone: '+08:00',
})
module.exports = seq
